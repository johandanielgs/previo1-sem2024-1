/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 * Está clase representa una entidad Estudiante en la UFPS
 *
 * @author madarme
 */
public class Estudiante implements Comparable<Estudiante> {

    private String nombre;
    private long codigo;
    private float p1, p2, p3, exm;

    //Requisito 1:  Constructores
    /**
     * Esté es el constructor vacío de la entidad Estudiante
     */
    public Estudiante() {
    }

    /**
     * COnstructor con paramétros para crear un Estudiante en la UFPS
     *
     * @param nombre una cadena de caractéres que representa el nombre de un
     * estudiante
     * @param codigo un número long con el ID UFPS
     * @param p1 un número float con el valor del previo1
     * @param p2 un número float con el valor del previo2
     * @param p3 un número float con el valor del previo3
     * @param exm un número float con el valor del examen
     */
    public Estudiante(String nombre, long codigo, float p1, float p2, float p3, float exm) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.exm = exm;
    }

    //Requisito 2: getter y setter
    /**
     * Obtiene el nombre de un estudiante
     *
     * @return una cadena de caracteres con el nombre del estudiante
     */
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public float getP1() {
        return p1;
    }

    public void setP1(float p1) {
        this.p1 = p1;
    }

    public float getP2() {
        return p2;
    }

    public void setP2(float p2) {
        this.p2 = p2;
    }

    public float getP3() {
        return p3;
    }

    public void setP3(float p3) {
        this.p3 = p3;
    }

    public float getExm() {
        return exm;
    }

    public void setExm(float exm) {
        this.exm = exm;
    }

    //Requisito 3:
    @Override
    public String toString() {
        return "Estudiante{" + "nombre=" + nombre + ", codigo=" + codigo + ", p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + ", exm=" + exm + '}';
    }

    //Requisito 4:
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + (int) (this.codigo ^ (this.codigo >>> 32));
        return hash;
    }

    /**
     * Obtiene si dos estudiantes son iguales por su nota definitiva
     *
     * @param obj el estudiante dos que se desea comparar
     * @return true si son iguales, o false en caso contario
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        return this.getPromedio() == other.getPromedio();
    }

    public float getPromedio() {
        return ((this.p1 + this.p2 + this.p3) / 3 * 0.7F + this.exm * 0.3F);
    }

    @Override
    public int compareTo(Estudiante o) {

        float rta = this.getPromedio() - o.getPromedio();
        if (rta == 0) {
            return 0;
        }
        if (rta < 0) {
            return -1;
        }
        return 1;
    }

}
