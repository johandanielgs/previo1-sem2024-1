/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.ListaS;

/**
 *
 * @author coloque acá sus nombres completos
 */
public class TestLista {
    
    public static void main(String[] args) {
        
        ListaS<Float> L1 = new ListaS();
        L1.insertarFin(5.0F);
        L1.insertarFin(6.0F);
        L1.insertarFin(4.0F);
        L1.insertarFin(3.0F);
        L1.insertarFin(1.0F);
        
        L1.deleteError();
        ListaS<Float> L2 = new ListaS();
        L2.insertarFin(5.0F);
        L2.insertarFin(6.0F);
        L2.insertarFin(7.0F);
        L2.insertarFin(4.0F);
        L2.insertarFin(1.0F);
        
        L2.deleteError();
    }
    
}
